package br.ufrgs.bank;

import br.ufrgs.bank.business.BusinessException;
import br.ufrgs.bank.business.domain.*;
import br.ufrgs.bank.business.impl.AccountOperationServiceImpl;
import br.ufrgs.bank.data.Database;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Date;

/**
 * Created by diogo on 6/18/14.
 */
public class AccountOperationServiceImplTest extends TestCase {

    public AccountOperationServiceImplTest(String testName) {
        super(testName);
    }

    @Before
    protected void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    public void testDeposit() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            assertEquals(deposit.getAmount(), 100D);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void testDepositZero() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit = accountOperationService.deposit(1L, 1L, 1L, 1L, 0D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testDepositNegative() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit = accountOperationService.deposit(1L, 1L, 1L, 1L, -10D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testGetBalance() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit1 = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            Deposit deposit2 = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            assertEquals(accountOperationService.getBalance(1L, 1L), 200D);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void testWithdrawalAllowedAmount() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit1 = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            accountOperationService.withdrawal(1L, 1L, 1L, 90D);
            assertEquals(accountOperationService.getBalance(1L, 1L), 10D);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void testWithdrawalLimitAmount() {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);
        try {
            Deposit deposit1 = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            accountOperationService.withdrawal(1L, 1L, 1L, 100D);
            assertEquals(accountOperationService.getBalance(1L, 1L), 0D);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void testWithdrawalBeyondLimitAmount() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        Deposit deposit1 = accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
        try {
            accountOperationService.withdrawal(1L, 1L, 1L, 110D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testWithdrawalBeyondLimitAmountWithZero() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        try {
            accountOperationService.withdrawal(1L, 1L, 1L, 10D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testWithdrawalZero() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        try {
            accountOperationService.withdrawal(1L, 1L, 1L, 0D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testWithdrawalNegative() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        try {
            accountOperationService.withdrawal(1L, 1L, 1L, -10D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }

    public void testTransferPositiveValue() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 2L))).
                thenReturn(new CurrentAccount(new Branch(1L), 2L, new Client("name", "lname", 2, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        try {
            accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            accountOperationService.deposit(1L, 1L, 2L, 1L, 100D);
            accountOperationService.transfer(1L, 1L, 1L, 1L, 2L, 10D);
            assertEquals(accountOperationService.getBalance(1L, 1L), 90D);
            assertEquals(accountOperationService.getBalance(1L, 2L), 110D);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void testTransferMoreThanPossible() throws BusinessException {
        Database databaseMock = Mockito.mock(Database.class);
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 1L))).
                thenReturn(new CurrentAccount(new Branch(1L), 1L, new Client("name", "lname", 1, "pass", new Date())));
        Mockito.when(databaseMock.getCurrentAccount(new CurrentAccountId(new Branch(1L), 2L))).
                thenReturn(new CurrentAccount(new Branch(1L), 2L, new Client("name", "lname", 2, "pass", new Date())));
        Mockito.when(databaseMock.getOperationLocation(1L)).thenReturn(new Branch(1L));
        AccountOperationServiceImpl accountOperationService = new AccountOperationServiceImpl(databaseMock);

        try {
            accountOperationService.deposit(1L, 1L, 1L, 1L, 100D);
            accountOperationService.deposit(1L, 1L, 2L, 1L, 100D);
            accountOperationService.transfer(1L, 1L, 1L, 1L, 2L, 110D);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), BusinessException.class);
        }
    }
}
