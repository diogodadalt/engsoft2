package br.ufrgs.bank.ui.text;

import br.ufrgs.bank.business.AccountOperationService;
import br.ufrgs.bank.business.domain.ATM;
import br.ufrgs.bank.business.domain.CurrentAccount;
import br.ufrgs.bank.ui.text.command.BalanceCommand;
import br.ufrgs.bank.ui.text.command.ClientLoginCommand;
import br.ufrgs.bank.ui.text.command.DepositCommand;
import br.ufrgs.bank.ui.text.command.LogoutCommand;
import br.ufrgs.bank.ui.text.command.StatementCommand;
import br.ufrgs.bank.ui.text.command.TransferCommand;
import br.ufrgs.bank.ui.text.command.WithdrawalCommand;

/**
 * @author Ingrid Nunes
 * 
 */
public class ATMInterface extends BankTextInterface {

	public ATMInterface(ATM atm, AccountOperationService accountOperationService) {
		super(atm);
		this.addAction("L", new ClientLoginCommand(this,
				accountOperationService));
		this.addAction("B", new BalanceCommand(this, accountOperationService));
		this.addAction("S", new StatementCommand(this, accountOperationService));
		this.addAction("D", new DepositCommand(this, accountOperationService));
		this.addAction("W",
				new WithdrawalCommand(this, accountOperationService));
		this.addAction("T", new TransferCommand(this, accountOperationService));
		this.addAction("O", new LogoutCommand(this));
	}

	@Override
	public Long readBranchId() {
		return isLoggedIn() ? ((CurrentAccount) getCredentials()).getId()
				.getBranch().getNumber() : 0;
	}

	@Override
	public Long readCurrentAccountNumber() {
		return isLoggedIn() ? ((CurrentAccount) getCredentials()).getId()
				.getNumber() : 0;
	}

}
