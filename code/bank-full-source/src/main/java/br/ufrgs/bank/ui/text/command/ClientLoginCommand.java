package br.ufrgs.bank.ui.text.command;

import br.ufrgs.bank.business.AccountOperationService;
import br.ufrgs.bank.business.domain.CurrentAccount;
import br.ufrgs.bank.ui.text.BankTextInterface;
import br.ufrgs.bank.ui.text.UIUtils;

/**
 * @author Ingrid Nunes
 * 
 */
public class ClientLoginCommand extends Command {

	private final AccountOperationService accountOperationService;

	public ClientLoginCommand(BankTextInterface bankInterface,
			AccountOperationService accountOperationService) {
		super(bankInterface);
		setEnabled(true);
		this.accountOperationService = accountOperationService;
	}

	@Override
	public void execute() throws Exception {
		Long branch = UIUtils.INSTANCE.readLong("branch");
		Long accountNumber = UIUtils.INSTANCE.readLong("account.number");
		String password = UIUtils.INSTANCE.readString("password");

		CurrentAccount currentAccount = accountOperationService.login(branch,
				accountNumber, password);
		bankInterface.login(currentAccount);
	}

}
