package br.ufrgs.bank.ui.text.command;

import br.ufrgs.bank.business.AccountManagementService;
import br.ufrgs.bank.business.domain.Employee;
import br.ufrgs.bank.ui.text.BankTextInterface;
import br.ufrgs.bank.ui.text.UIUtils;

/**
 * @author Ingrid Nunes
 * 
 */
public class EmployeeLoginCommand extends Command {

	private final AccountManagementService accountManagementService;

	public EmployeeLoginCommand(BankTextInterface bankInterface,
			AccountManagementService accountManagementService) {
		super(bankInterface, true);
		this.accountManagementService = accountManagementService;
	}

	@Override
	public void execute() throws Exception {
		String username = UIUtils.INSTANCE.readString("username");
		String password = UIUtils.INSTANCE.readString("password");

		Employee employee = accountManagementService.login(username, password);
		bankInterface.login(employee);
	}

}
