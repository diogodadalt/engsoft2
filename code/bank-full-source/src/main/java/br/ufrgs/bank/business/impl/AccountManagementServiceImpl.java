/*
 * Created on 5 Jan 2014 00:51:19 
 */
package br.ufrgs.bank.business.impl;

import java.util.Date;

import br.ufrgs.bank.business.AccountManagementService;
import br.ufrgs.bank.business.BusinessException;
import br.ufrgs.bank.business.domain.Branch;
import br.ufrgs.bank.business.domain.Client;
import br.ufrgs.bank.business.domain.CurrentAccount;
import br.ufrgs.bank.business.domain.Employee;
import br.ufrgs.bank.business.domain.OperationLocation;
import br.ufrgs.bank.data.Database;
import br.ufrgs.bank.util.RandomString;

/**
 * @author Ingrid Nunes
 * 
 */
public class AccountManagementServiceImpl implements AccountManagementService {

	private final Database database;
	private RandomString random;

	public AccountManagementServiceImpl(Database database) {
		this.database = database;
		this.random = new RandomString(8);
	}

	@Override
	public CurrentAccount createCurrentAccount(long branch, String name,
			String lastName, int cpf, Date birthday, double balance)
			throws BusinessException {
		OperationLocation operationLocation = database
				.getOperationLocation(branch);
		if (operationLocation == null || !(operationLocation instanceof Branch)) {
			throw new BusinessException("exception.invalid.branch");
		}

		Client client = new Client(name, lastName, cpf, random.nextString(),
				birthday);
		CurrentAccount currentAccount = new CurrentAccount(
				(Branch) operationLocation,
				database.getNextCurrentAccountNumber(), client, balance);

		database.save(currentAccount);

		return currentAccount;
	}

	@Override
	public Employee login(String username, String password)
			throws BusinessException {
		Employee employee = database.getEmployee(username);

		if (employee == null) {
			throw new BusinessException("exception.inexistent.employee");
		}
		if (!employee.getPassword().equals(password)) {
			throw new BusinessException("exception.invalid.password");
		}

		return employee;
	}

}
