/*
 * Created on 7 Jan 2014 21:21:55 
 */
package br.ufrgs.bank;

import br.ufrgs.bank.business.AccountManagementService;
import br.ufrgs.bank.business.domain.ATM;
import br.ufrgs.bank.business.domain.Branch;
import br.ufrgs.bank.business.impl.AccountOperationServiceImpl;
import br.ufrgs.bank.ui.BankInterface;
import br.ufrgs.bank.ui.graphic.ATMGUIInterface;
import br.ufrgs.bank.ui.graphic.BranchGUIInterface;
import br.ufrgs.bank.ui.text.UIUtils;

/**
 * @author ingrid
 * 
 */
public class BankGraphic extends Bank {

	@Override
	public BankInterface createATMInterface(ATM atm,
			AccountOperationServiceImpl accountOperationService) {
		return new ATMGUIInterface(atm, UIUtils.INSTANCE.getTextManager(),
				accountOperationService);
	}

	@Override
	public BankInterface createBranchInterface(Branch branch,
			AccountManagementService accountManagementService,
			AccountOperationServiceImpl accountOperationService) {
		return new BranchGUIInterface(branch,
				UIUtils.INSTANCE.getTextManager(), accountManagementService,
				accountOperationService);
	}

	public void showUI() {
		for (BankInterface bankInterface : bankInterfaces) {
			bankInterface.createAndShowUI();
		}
	}

}
